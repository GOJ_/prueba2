DECLARE @name VARCHAR(50) -- database name  
DECLARE @path VARCHAR(256) -- path for backup files  
DECLARE @fileName VARCHAR(256) -- filename for backup  
DECLARE @fileDate VARCHAR(20) -- used for file name
 
-- specify database backup directory
SET @path = 'C:\Backup\'  
 
-- specify filename format
SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112) 
 
DECLARE db_cursor CURSOR READ_ONLY FOR  
SELECT name 
FROM master.dbo.sysdatabases 
-- WHERE name IN ('Northwind')
WHERE name IN ('Northwind', 'AdventureWorks2014')
-- WHERE name NOT IN ('master','model','msdb','tempdb')  -- exclude these databases
 
OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @name   
 
WHILE @@FETCH_STATUS = 0   
BEGIN   
   SET @fileName = @path + @name + '_' + @fileDate + '.BAK'  
   BACKUP DATABASE @name TO DISK = @fileName  
 
   FETCH NEXT FROM db_cursor INTO @name   
END   

 
CLOSE db_cursor   
DEALLOCATE db_cursor
-- End Script




--Processed 24328 pages for database 'AdventureWorks2014', file 'AdventureWorks2014_Data' on file 1.
--Processed 2 pages for database 'AdventureWorks2014', file 'AdventureWorks2014_Log' on file 1.
--BACKUP DATABASE successfully processed 24330 pages in 8.321 seconds (22.842 MB/sec).
--Processed 864 pages for database 'Northwind', file 'Northwind' on file 1.
--Processed 2 pages for database 'Northwind', file 'Northwind_log' on file 1.
--BACKUP DATABASE successfully processed 866 pages in 0.489 seconds (13.820 MB/sec).


--File Naming Format DBname_YYYYDDMM_HHMMSS.BAK
--If you want to also include the time in the filename you can replace this line in the above script:

---- specify filename format
--SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112)
--with this line:

---- specify filename format
--SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112) + REPLACE(CONVERT(VARCHAR(20),GETDATE(),108),':','')